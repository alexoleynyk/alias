//
//  AliasTests.swift
//  AliasTests
//
//  Created by Alex Oleynyk on 2/13/19.
//  Copyright © 2019 Alex Oleynyk. All rights reserved.
//

import XCTest

class AliasTests: XCTestCase {

    func test_dataController() {
        let sut = ArrayDataController(withData: [1, 2, 3])
        
        print(sut.randomItem)
        print(sut.randomItem)
        print(sut.randomItem)
        print(sut.randomItem)
        print(sut.randomItem)
    }
    
    func test_dataSavedController() {
        let sut = ArrayDataSavingController(withData: [1, 2, 3])
        
        print(sut.randomItem)
        sut.saveLast()
        print(sut.randomItem)
        sut.saveLast()
        sut.update(atIndex: 1)
        sut.update(atIndex: 1)
        sut.update(atIndex: 1)
        print(sut.randomItem)
        print(sut.randomItem)
        print(sut.randomItem)
    }
    
}
