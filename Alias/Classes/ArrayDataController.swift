//
//  ArrayDataController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 16.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation

class ArrayDataController<Element: Codable> : CustomStringConvertible, Codable  {
    //нужно сделать этот класс универсальным, чтобы он работал с любыми данными
    var dataArray: [Element] = []
    var usedData: [Element] = []
    
    var randomItem: Element? {
        get {
            
            var count = self.dataArray.count
            if count == 0 {
                if usedData.count == 0 {
                    return nil
                }
                
                dataArray = usedData
                usedData = []
                count = self.dataArray.count
            }
            
            let randomIndex = Int(arc4random_uniform(UInt32(count)))
            
            
            let item = self.dataArray[randomIndex]
            dataArray.remove(at: randomIndex)
            usedData.append(item)
            return item
        }
    }
    
    var description: String {
        let data = dataArray+usedData
        return "\(data)"
    }
    
    
    init(withData data:[Element]) {
        self.dataArray = data
    }

    
}

class ArrayDataSavingController<Element : Codable> : Codable{
    
    var savedData: [Element] = []
    var dataArray: [Element] = []
    var usedData: [Element] = []
    
    var randomItem: Element? {
        get {
            
            var count = self.dataArray.count
            if count == 0 {
                if usedData.count == 0 {
                    return nil
                }
                
                dataArray = usedData
                usedData = []
                count = self.dataArray.count
            }
            
            let randomIndex = Int(arc4random_uniform(UInt32(count)))
            
            
            let item = self.dataArray[randomIndex]
            dataArray.remove(at: randomIndex)
            usedData.append(item)
            return item
        }
    }
    
    var description: String {
        let data = dataArray+usedData
        return "\(data)"
    }
    
    
    init(withData data:[Element]) {
        self.dataArray = data
    }
    
    
    func saveLast() {
        savedData.append(usedData.removeLast())
    }
    
    func update(atIndex index: Int) -> Element? {
        if let newItem = randomItem {
            savedData.insert(newItem, at: index)
            usedData.append(savedData.remove(at: index))
            return newItem
        }
        
        return nil
//        print(dataArray)
//        print(usedData)
//        print(savedData)
//        print(index)
        
//        usedData.remove(at: index)
    }
//
//    func deleteLast() {
//        usedData.append(savedData.removeLast())
//    }
    
//    func updateLast() {
//        usedData.append(savedData.removeLast())
//        let _ = randomItem
//        savedData.append(usedData.removeLast())
//    }
    
    func delete(atIndex index: Int) {
        usedData.append(savedData[index])
        savedData.remove(at: index)
    }
    

}
