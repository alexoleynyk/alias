//
//  WordPack.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 22.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation




struct WordsPack: Decodable {
    let packName: String?
    let packDescription: String?
    let words: [String]?
}

struct WordsPackParse: Decodable {
    let packName: String
    let packDescription: String
    let words: [String]
}



