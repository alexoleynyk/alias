//
//  DataManager.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 16.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation
import AVFoundation

class DataManager {
    
    
    static func loadWordPacks() -> [WordsPack] {
//        let wordsPackNames = ["Optimal", "Pack"]
        let wordsPackNames = Globals.packsInGame
        
        var result: [WordsPack] = []
        
        for packName in wordsPackNames {
//            print(packName)
            let path = Bundle.main.path(forResource: packName, ofType: "json")
            let url = URL(fileURLWithPath: path!)
            print(url)
            do {
                let data = try Data(contentsOf: url)
                let pack = try JSONDecoder().decode(WordsPackParse.self, from: data)
//                print(pack)
                result.append(WordsPack(packName: pack.packName, packDescription: pack.packDescription, words: pack.words))
            } catch {
                print("\(error)")
                continue
            }
        }
        
        return result

        
    }
    
    static func loadTeams() -> [Team]{
        let path = Bundle.main.path(forResource: "Teams", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        
        do {
            let data = try Data(contentsOf: url)
            let teams = try JSONDecoder().decode([String].self, from: data)
            let result = teams.map {return Team(name: $0)}
            return result
        } catch {
            print("\(error)")
            return []
        }
    }
    
    static func loadQuests() -> [String]{
        let path = Bundle.main.path(forResource: "Quests", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        
        do {
            let data = try Data(contentsOf: url)
            let quests = try JSONDecoder().decode([String].self, from: data)
//            print(quests[15])
            return quests
        } catch {
            print("\(error)")
            return []
        }
    }
    
    
    static func loadSettings() -> GameSettings {
        let gameSettings = GameSettings()
        
        if let wordsCountForWin = UserDefaults.standard.value(forKey: "wordsCountForWin") as? Int {
            gameSettings.wordsForWin = wordsCountForWin
        }
        
        if let timeRound = UserDefaults.standard.value(forKey: "timeRound") as? Int {
            gameSettings.roundDuration = timeRound
        }
        
        if let questFrequance = UserDefaults.standard.value(forKey: "questFrequance") as? String {
            gameSettings.questFrequency = GameSettings.QuestFrequency(rawValue: questFrequance)!
        }
        
        if let isMusicOn = UserDefaults.standard.value(forKey: "isMusicOn") as? Bool {
            gameSettings.isMusicOn = isMusicOn
        }
        
        if let isLastWordForAll = UserDefaults.standard.value(forKey: "isLastWordForAll") as? Bool {
            gameSettings.isLastWordForAll = isLastWordForAll
        }
        
        if let isWordPenaltyOn = UserDefaults.standard.value(forKey: "isWordPenaltyOn") as? Bool {
            gameSettings.isWordPenaltyOn = isWordPenaltyOn
        }
        
        
        
        return gameSettings
        
        
    }
    
    static func saveSettings(_ gameSettings: GameSettings) {
        print(gameSettings)
        UserDefaults.standard.set(gameSettings.wordsForWin, forKey: "wordsCountForWin")
        UserDefaults.standard.set(gameSettings.roundDuration, forKey: "timeRound")
        UserDefaults.standard.set(gameSettings.questFrequency.rawValue, forKey: "questFrequance")
        UserDefaults.standard.set(gameSettings.isMusicOn, forKey: "isMusicOn")
        UserDefaults.standard.set(gameSettings.isLastWordForAll, forKey: "isLastWordForAll")
        UserDefaults.standard.set(gameSettings.isWordPenaltyOn, forKey: "isWordPenaltyOn")
    }
    
    static func saveState(_ state: GameState) {
        
        if let encodedData = try? JSONEncoder().encode(state) {
//            let path = Bundle.main.path(forResource: "State", ofType: "json")
//            let url = URL(fileURLWithPath: path!)
            let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
            let ArchiveURL = DocumentsDirectory.appendingPathComponent("state")
            do {
                try encodedData.write(to: ArchiveURL)
                print("Success")
                print(ArchiveURL)
            }
            catch {
                print("Failed to write JSON data: \(error.localizedDescription)")
            }
        }
        
        
    }
    
    static func loadState() -> GameState? {
        var result: GameState? = nil
        
        let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        let ArchiveURL = DocumentsDirectory.appendingPathComponent("state")
        do {
            let data = try Data(contentsOf: ArchiveURL)
            result  = try JSONDecoder().decode(GameState.self, from: data)
        } catch {
            print("my error: \(error)")
            return nil
        }
        return result
    }
    
    static func deleteSavedState() {
        let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        let ArchiveURL = DocumentsDirectory.appendingPathComponent("state")
        do {
            try FileManager().removeItem(at: ArchiveURL)
//            result  = try JSONDecoder().decode(GameState.self, from: data)
        } catch {
            print("\(error)")
//            return nil
        }
    }
    
    static func loadSound(name: String, type: String) -> AVAudioPlayer {
        
        let path = Bundle.main.path(forResource: name, ofType: type)
        let url = URL(fileURLWithPath: path!)
        
        do {
            let player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
            return player
        } catch {
            print(error)
        }
        
        return AVAudioPlayer()
    }
    
    static func loadGameWins() -> Int {
        if let gameWins = UserDefaults.standard.value(forKey: "gameWins") as? Int {
            return gameWins
        } else {
            return 0
        }
    }
    
    static func updateGameWins() {
        Globals.gameWins += 1
        UserDefaults.standard.set(Globals.gameWins, forKey: "gameWins")
    }
    
    static func loadRatingRequest() -> Bool {
        if let ratingRequest = UserDefaults.standard.value(forKey: "ratingRequest") as? Bool {
            return ratingRequest
        } else {
            return false
        }
    }
    
    static func updateRatingRequest() {
        Globals.ratingRequestRejected = true
        UserDefaults.standard.set(true, forKey: "ratingRequest")
    }
}
