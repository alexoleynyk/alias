//
//  Constants.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 28.04.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation
import UIKit

struct Globals {
    
    static let packsInGame = ["BaseAlias", "QuickGame", "Normal", "Hardcore", "Phraseological", "Summer"] // "NewYear", 

    // Colors
    static let yellowColor = UIColor(red: 243/255, green: 192/255, blue: 7/255, alpha: 1).cgColor
    static let darkBlueColor = UIColor(red: 0/255, green: 58/255, blue: 95/255, alpha: 1).cgColor
    static let lightBlueColor = UIColor(red: 0/255, green: 139/255, blue: 239/255, alpha: 1).cgColor
    
    static var gameRoundLoopCount = 0
    static var gameWins = 0
    static var ratingRequestRejected = false
}
