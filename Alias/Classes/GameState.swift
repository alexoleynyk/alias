//
//  GameState.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 22.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation


class GameState: Codable {
    var teamInGame: [Team] = []
    var currentTeamIndex: Int = 0
    var wordsInGame: ArrayDataController<String>? = nil
    
}
