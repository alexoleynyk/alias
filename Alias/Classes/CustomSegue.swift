//
//  customSegue.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 11.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class CustomSegue: UIStoryboardSegue {

    override func perform() {
        fadeIn()
    }
    
    func fadeIn() {
        let fromVC = source
        let toVC = destination
        
        
        let originalCenter = fromVC.view.center

        toVC.view.center = originalCenter
        toVC.view.alpha = 0
        
//        let container = fromVC.view.superview
//        container?.addSubview(toVC.view)
        
        var subViews = fromVC.view.subviews
        subViews.removeFirst()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
            
            for subView in subViews {
                subView.alpha = 0
            }
            
        }, completion: { success in
            let window = UIApplication.shared.keyWindow
            window?.insertSubview(toVC.view, aboveSubview: fromVC.view)
            
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
                toVC.view.alpha = 1
                
            }, completion: { success in
                
                fromVC.view.alpha = 0
                fromVC.present(toVC, animated: false, completion: {
                    if let VC = toVC as? TeamsSettingViewController {
                        toVC.startBoounce(for: VC.viewsToBounce)
                    }
                })
                
            })
        })
        
    }
    
}

protocol GameControllerTransfer {
    var gameController: String {set get}
    var gameState: GameState? {set get}
//    var gameController: GameController
}
