//
//  BounceButton.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 21.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class BounceButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        makeBounce()
//        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        makeAnotherBounce()
        super.touchesBegan(touches, with: event)
        super.touchesEnded(touches, with: event)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    

    
    private func makeBounce(){
        //        view.transform = CGAffineTransform(scaleX: 1, y: 1)
        UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (success) in
//            makeAnotherBounce()
        }
    }
    
    func makeBounceCall(isSelected: Bool){
        //        view.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.isSelected = isSelected
        UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (success) in
//            self.isSelected = isSelected
            self.makeAnotherBounce()
        }
    }
    
        
    func makeAnotherBounce(){
        //            view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        UIView.animate(withDuration: 0.1, delay: 0, options: [.allowUserInteraction, .curveLinear], animations: {
            self.transform = CGAffineTransform(scaleX: 1.08, y: 1.08)
        }) { (success) in
            self.makeAnotherOneBounce()
        }
    }
    
    func makeAnotherOneBounce(){
        UIView.animate(withDuration: 0.1, delay: 0, options: [.allowUserInteraction, .curveLinear], animations: {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }) { (success) in
            
        }
    }
    
    

}
