//
//  CustomSegueUnwind.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 21.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class CustomSegueUnwind: UIStoryboardSegue {
    override func perform() {
        fadeIn()
    }
    
    func fadeIn() {
        let fromVC = source
        let toVC = destination
        
        let container = fromVC.view.superview
        let originalCenter = fromVC.view.center
        
        toVC.view.center = originalCenter
        toVC.view.alpha = 0
        
        container?.addSubview(toVC.view)
        
        var subViews = fromVC.view.subviews
        _ = subViews.removeFirst()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
            
            for subView in subViews {
                subView.alpha = 0
            }
            
        }, completion: { success in
            let window = UIApplication.shared.keyWindow
            window?.insertSubview(toVC.view, aboveSubview: fromVC.view)
            
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
                toVC.view.alpha = 1
                
                for subView in toVC.view.subviews {
                    subView.alpha = 1
                }
            }, completion: { success in
                fromVC.view.alpha = 0
                toVC.dismiss(animated: false, completion: {
                    print("unwind done")
                    if let VC = toVC as?  StartScreenViewController {
                        toVC.startBoounce(for: VC.viewToBouns)
                    } 
                    
                })
            })
        })
        
        
    }

}

