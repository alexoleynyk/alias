//
//  Team.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 16.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation


struct Team : Decodable, Encodable {
    let teamName: String
    var teamDescription: String? = ""
    var score: Int? = 0
    
    init(name teamName: String) {
        self.teamName = teamName
    }
    
    init(name teamName: String, _ description: String){
        self.teamName = teamName
        self.teamDescription = description
    }
    
//    var description: String {
//        let result = "\(self.teamName) " + (!teamDescription.isEmpty ? " - \(teamDescription)" : "") + ": \(self.score ?? 0)"
//        return result
//    }
}
