//
//  GameSettings.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 16.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import Foundation



class GameSettings {
    
    enum QuestFrequency: String {
        case never = "Никогда", rarely = "Редко", often = "Часто", always = "Всегда"
    }
    
    var roundDuration: Int = 60
    var wordsForWin: Int = 100
    var isLastWordForAll: Bool = false
    var isMusicOn: Bool = false
    var isWordPenaltyOn: Bool = false
    var isQuestPenaltyOn: Bool = false
    var questFrequency: QuestFrequency = .rarely
    
    let minimumWords = 10
    let maximumWords = 200
    let minimumRoundDuration = 30
    let maximumRoundDuration = 300
    
    func changeRoundDuration(by seconds: Int) {
        let result = self.roundDuration + seconds
        if (result >= minimumRoundDuration && result <= maximumRoundDuration) {
            self.roundDuration += seconds
        }
    }
    
    func changeWordsForWin(by amount: Int) {
        let result = self.wordsForWin + amount
        if (result >= minimumWords && result <= maximumWords) {
            self.wordsForWin += amount
        }
    }
    
    func increaseQuestFrequency() {
        switch self.questFrequency {
        case .never:
            self.questFrequency = .rarely
        case .rarely:
            self.questFrequency = .often
        case .often, .always:
            self.questFrequency = .always
        }
    }
    
    func decreaseQuestFrequency() {
        switch self.questFrequency {
        case .never, .rarely:
            self.questFrequency = .never
        case .often:
            self.questFrequency = .rarely
        case .always:
            self.questFrequency = .often
        }
    }
    
    func switchLastWordForAll() {
        self.isLastWordForAll = !self.isLastWordForAll
    }
    
    func switchMusicOn() {
        self.isMusicOn = !self.isMusicOn
    }
    
    func showData() {
        print("roundDuration: \(roundDuration), wordsForWin: \(wordsForWin), isLastWordForAll: \(isLastWordForAll), questFrequency: \(questFrequency),")
    }
    
    
}
