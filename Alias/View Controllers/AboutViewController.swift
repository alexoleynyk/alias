//
//  AboutViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 30.04.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent("about_page_show", parameters: nil)
    }
    
    @IBAction func sendMailButtonPressed(_ sender: BounceButton) {
        
        let email = "alex.oleynyk@gmail.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
