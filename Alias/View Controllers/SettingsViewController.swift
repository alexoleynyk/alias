//
//  SettingsViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 18.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class SettingsViewController: UIViewController {
    
    // MARK: - Types
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var wordsPackLabel: UILabel!
    @IBOutlet weak var wordsCountForWinLabel: UILabel!
    @IBOutlet weak var timeRoundLabel: UILabel!
    @IBOutlet weak var questFrequanceLabel: UILabel!
    
    @IBOutlet weak var wordPackView: UIView!
    
    @IBOutlet weak var isMusicOnButton: UIButton!
    @IBOutlet weak var isLastWordForAllButton: UIButton!
    @IBOutlet weak var isWordPenaltyOnButton: UIButton!
    
    @IBOutlet weak var lessWordsButton: UIButton!
    @IBOutlet weak var moreWordsButton: UIButton!
    @IBOutlet weak var lessTimeButton: UIButton!
    @IBOutlet weak var moreTimeButton: UIButton!
    @IBOutlet weak var lessQuestsButton: UIButton!
    @IBOutlet weak var moreQuestsButton: UIButton!
    
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet var buttons: [UIButton]!
    
    // MARK: - Public Properties
    
    var gameSettings: GameSettings = GameSettings()
    var gameState: GameState? = nil
    
    var selectedPackId = -1 {
        didSet {
            guard selectedPackId != -1 else {
                return
            }
//            wordsPackLabel.text? = wordsPacks![selectedPackId].packName!
//            startButton.isEnabled = true
        }
    }
    
    var wordsPacks: [WordsPack]? = nil
    
    
    // MARK: - Private Properties
    
    // MARK: - Initializers
    
    // MARK: - UIViewController
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

//        setTapGestureRecognizer(forView: wordPackView)
        
        gameSettings = DataManager.loadSettings()
        wordsPacks = DataManager.loadWordPacks()
        
        if gameSettings.wordsForWin == gameSettings.minimumWords {
            lessWordsButton.isEnabled = false
        }
        if gameSettings.wordsForWin == gameSettings.maximumWords {
            moreWordsButton.isEnabled = false
        }
        if gameSettings.roundDuration == gameSettings.minimumRoundDuration {
            lessTimeButton.isEnabled = false
        }
        if gameSettings.roundDuration == gameSettings.maximumRoundDuration {
            moreTimeButton.isEnabled = false
        }
        if gameSettings.questFrequency == GameSettings.QuestFrequency.never {
            lessQuestsButton.isEnabled = false
        }
        if gameSettings.questFrequency == GameSettings.QuestFrequency.always {
            moreQuestsButton.isEnabled = false
        }
        
        
        wordsCountForWinLabel.text? = "\(gameSettings.wordsForWin)"
        timeRoundLabel.text? = "\(gameSettings.roundDuration)"
        questFrequanceLabel.text? = "\(gameSettings.questFrequency.rawValue)"
        isMusicOnButton.isSelected = gameSettings.isMusicOn
        isLastWordForAllButton.isSelected = gameSettings.isLastWordForAll
        isWordPenaltyOnButton.isSelected = gameSettings.isWordPenaltyOn
        
    }
    
    
    // MARK: - Public methods
    
    // MARK: - IBActions
    
    @IBAction func lessWords(_ sender: UIButton) {
        if gameSettings.wordsForWin == gameSettings.maximumWords {
            moreWordsButton.isEnabled = true
        }
        
        gameSettings.changeWordsForWin(by: -10)
        wordsCountForWinLabel.text? = "\(gameSettings.wordsForWin)"
        
        if gameSettings.wordsForWin == gameSettings.minimumWords {
            lessWordsButton.isEnabled = false
        }
        
    }
    
    @IBAction func moreWords(_ sender: UIButton) {
        if gameSettings.wordsForWin == gameSettings.minimumWords {
            lessWordsButton.isEnabled = true
        }
        
        gameSettings.changeWordsForWin(by: 10)
        wordsCountForWinLabel.text? = "\(gameSettings.wordsForWin)"
        
        if gameSettings.wordsForWin == gameSettings.maximumWords {
            moreWordsButton.isEnabled = false
        }
    }
    
    @IBAction func lessTime(_ sender: UIButton) {
        
        if gameSettings.roundDuration == gameSettings.maximumRoundDuration {
            moreTimeButton.isEnabled = true
        }
        
        gameSettings.changeRoundDuration(by: -30)
        timeRoundLabel.text? = "\(gameSettings.roundDuration)"
        
        if gameSettings.roundDuration == gameSettings.minimumRoundDuration {
            lessTimeButton.isEnabled = false
        }
        
        
    }
    @IBAction func moreTime(_ sender: UIButton) {
        if gameSettings.roundDuration == gameSettings.minimumRoundDuration {
            lessTimeButton.isEnabled = true
        }
        
        gameSettings.changeRoundDuration(by: 30)
        timeRoundLabel.text? = "\(gameSettings.roundDuration)"
        
        if gameSettings.roundDuration == gameSettings.maximumRoundDuration {
            moreTimeButton.isEnabled = false
        }
    }
    
    @IBAction func lessQuests(_ sender: UIButton) {
        if gameSettings.questFrequency == GameSettings.QuestFrequency.always {
            moreQuestsButton.isEnabled = true
        }
        
        gameSettings.decreaseQuestFrequency()
        questFrequanceLabel.text? = "\(gameSettings.questFrequency.rawValue)"
        
        if gameSettings.questFrequency == GameSettings.QuestFrequency.never {
            lessQuestsButton.isEnabled = false
        }
    }
    
    @IBAction func moreQuests(_ sender: UIButton) {
        if gameSettings.questFrequency == GameSettings.QuestFrequency.never {
            lessQuestsButton.isEnabled = true
        }
        
        gameSettings.increaseQuestFrequency()
        questFrequanceLabel.text? = "\(gameSettings.questFrequency.rawValue)"
        
        if gameSettings.questFrequency == GameSettings.QuestFrequency.always {
            moreQuestsButton.isEnabled = false
        }
    }
    
    @IBAction func isMusicOnChange(_ sender: UIButton) {
        // упростить метод
        let isMusicOn = gameSettings.isMusicOn
//        print("state before \(isMusicOn)")
        gameSettings.isMusicOn = !isMusicOn
        isMusicOnButton.isSelected = !isMusicOn
        
    }
    
    
    @IBAction func isLastWordForAllChange(_ sender: UIButton) {
        // упростить метод
        let isLastWordForAll = gameSettings.isLastWordForAll
//        print("state before \(isLastWordForAll)")
        
        gameSettings.isLastWordForAll = !isLastWordForAll
        isLastWordForAllButton.isSelected = !isLastWordForAll
        
//        print("state now \(!isLastWordForAll)")
    }
    
    @IBAction func isWordPenaltyOnChange(_ sender: UIButton) {
        // упростить метод
        let isWordPenaltyOn = gameSettings.isWordPenaltyOn
//        print("state before \(isWordPenaltyOn)")
        
        gameSettings.isWordPenaltyOn = !isWordPenaltyOn
        isWordPenaltyOnButton.isSelected = !isWordPenaltyOn
        
//        print("state now \(!isWordPenaltyOn)")
    }
    
    // MARK: - Private Methods
    
    private func setTapGestureRecognizer(forView view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(singleTapped(_:)))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
    }
    
    @objc private func singleTapped(_ recognizer:UITapGestureRecognizer){
        performSegue(withIdentifier: "showWordPackVeiwController", sender: self)
    }
    
    
    // MARK: - Navigation

    @IBAction func unwindFromWordSelection(_ sender: UIStoryboardSegue) {
        if sender.source is WordPackPickViewController {
//            print("Hello from WordPick! O_o")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Если загружаем экран команд - передаем настройки + сохраняем их, и передаем состояние с добавленным набором слов
        if let dest = segue.destination as? TeamScoreViewController {

            DataManager.saveSettings(self.gameSettings)
            dest.gameSettings = self.gameSettings
//            gameState?.wordsInGame = ArrayDataController(withData: wordsPacks![selectedPackId].words!)
//            print(gameState?.wordsInGame ?? [])
            dest.gameState = gameState
            DataManager.saveState(gameState!)
            
            var params = [String: Any]()
            params["teams_count"] = "\(gameState!.teamInGame.count)"
            
            if selectedPackId != -1 && wordsPacks != nil {
                params["word_pack"] = "\(wordsPacks![selectedPackId].packName!)"
            }
            params["quest_frequency"] = "\(gameSettings.questFrequency.rawValue)"
            params["last_word_for_all"] = "\(gameSettings.isLastWordForAll)"
            params["round_time"] = "\(gameSettings.roundDuration)"
            params["word_penalty"] = "\(gameSettings.isWordPenaltyOn)"
            params["words_for_win"] = "\(gameSettings.wordsForWin)"
            
            Analytics.logEvent("game_started", parameters: params)
            
            
        // Если загружаем окно выбора слов - загружаем все наборы и передаем их
        } else if let dest = segue.destination as? WordPackPickViewController {
            print("go to WordPackPickViewController")
//            wordsPacks = DataManager.loadWordPacks()
//            print(wordsPacks![0].words)
//            dest.data = wordsPacks
        } else if let dest = segue.destination as? TeamsSettingViewController {
            dest.gameState = gameState
        }
    }

}
