//
//  TeamsSettingViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 18.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class TeamsSettingViewController: UIViewController {
   
    // MARK: - Types
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addTeamButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var viewsToBounce: [UIView]!
    
    // MARK: - Public Properties
    var gameState: GameState? = nil
    var teamsDataController: ArrayDataSavingController<String>? = nil
    var teamsDataSource: [String]? = nil
    var data:[String] = []
    
    // MARK: - Private Properties
    
    // MARK: - Initializers
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let teamsArray = DataManager.loadTeams().map { $0.teamName }
        teamsDataController = ArrayDataSavingController(withData: teamsArray)
//        print(teamsDataController?.dataArray)
        
        for _ in 0...1 {
            data.append((teamsDataController?.randomItem)!)
//            print(teamsDataController?.dataArray)
            teamsDataController?.saveLast()
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(removeTeam(_:)))
        swipe.direction = .left
        tableView.addGestureRecognizer(swipe)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(renameTeam(_:)))
        tap.numberOfTapsRequired = 1
        tableView.addGestureRecognizer(tap)
        
        nextButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    }
    
    // MARK: - Public methods
    
    // MARK: - IBActions
    
    @IBAction func addNewTeam(_ sender: UIButton) {
        addNewTeam()
    }
    
    // MARK: - Private Methods
    
    @objc private func removeTeam(_ recognizer:UITapGestureRecognizer){
        
        let viewTapped = recognizer.location(in: recognizer.view)
        if let tappedCellIndexPath = tableView.indexPathForRow(at: viewTapped){
            data.remove(at: tappedCellIndexPath.row)
            teamsDataController?.delete(atIndex: tappedCellIndexPath.row)
            tableView.deleteRows(at: [tappedCellIndexPath], with: .left)
//            print(data)
            
            if data.count < 2 {
                nextButton.isEnabled = false
            }
            
            if !(teamsDataController?.usedData.isEmpty)! || !(teamsDataController?.dataArray.isEmpty)! {
                addTeamButton.isEnabled  = true
            }
            
        }
        
    }
    
    @objc private func renameTeam(_ recognizer:UITapGestureRecognizer){
        
        let viewTapped = recognizer.location(in: recognizer.view)
        if let tappedCellIndexPath = tableView.indexPathForRow(at: viewTapped){
            let index = tappedCellIndexPath.row
            
            if let item = teamsDataController?.update(atIndex: index) {
                data.remove(at: index)
                data.insert(item, at: index)
                tableView.reloadRows(at: [tappedCellIndexPath], with: .fade)
            }
        }

    }
    
    private func addNewTeam() {
        if let item = teamsDataController?.randomItem {
            if data.count == 1 {
                nextButton.isEnabled = true
            }
            
            data.append(item)
            teamsDataController?.saveLast()
            tableView.insertRows(at: [IndexPath(row: data.count - 1, section: 0)], with: .fade)
            tableView.scrollToRow(at: IndexPath(row: data.count - 1, section: 0), at: .bottom, animated: true)
            

            
            if (data.count == 6) {
                addTeamButton.isEnabled  = false
            }
            
            
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? StartScreenViewController {
            dest.gameState = nil
        
        
        } else if let dest = segue.destination as? SettingsViewController {
            let gameState = GameState()
            let teamsInGameArray = data.map { Team(name: $0) }
            gameState.teamInGame = teamsInGameArray
            dest.gameState = gameState
        
        
        } else if let dest = segue.destination as? WordPackPickViewController {
            let gameState = GameState()
            let teamsInGameArray = data.map { Team(name: $0) }
            gameState.teamInGame = teamsInGameArray
            dest.gameState = gameState
        }
    }
    
    @IBAction func unwindFromSettings(_ sender: UIStoryboardSegue) {
        if sender.source is SettingsViewController {
            print("Hello from SettingView!")
        }
    }


}

extension TeamsSettingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell") as! TeamTableViewCell
        
        cell.nameLabel.text = String(data[indexPath.row])
        
        return cell
    }
}

extension TeamsSettingViewController: UITableViewDelegate {
    
}
