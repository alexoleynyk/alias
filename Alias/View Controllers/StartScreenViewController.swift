//
//  StartScreenViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 18.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Crashlytics
import StoreKit
import FirebaseAnalytics

class StartScreenViewController: UIViewController {
    
    // MARK: - Types
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var viewToBouns: [UIView]!
    @IBOutlet weak var continueButton: BounceButton!
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var modalContainer: UIView!
    @IBOutlet weak var modalView: UIView!
    
    // MARK: - Public Properties
    var gameState: GameState? = nil
    
    // MARK: - Private Properties
    private var interstitial:GADInterstitial!
    private var showAd = false
    // MARK: - Initializers
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Стартовая анимация кнопок
        startBoounce(for: viewToBouns)
        
        // TODO: - пофиксить обводку
        cardView.layer.borderWidth = 6
        cardView.layer.borderColor = Globals.yellowColor
        
        
//        interstitial = createAndLoadInterstitial()
        
        if gameState != nil { // перенес из след метода сюда
            continueButton.isHidden = false
        } else {
            continueButton.isHidden = true
        }
        
        
    }

    
    // MARK: - Public methods

    // MARK: - IBActions
    
    @IBAction func cancelNewGameButton(_ sender: UIButton) {
        
        hideModal()
    }
    
    @IBAction func confirmNewGameButton(_ sender: UIButton) {
        
        DataManager.deleteSavedState()
        hideModal()
    }
    
    @IBAction func startNewGameButtonPressed(_ sender: UIButton) {

        if gameState != nil {
            showModal()
        } else {
            performSegue(withIdentifier: "goToTeamSettings", sender: sender)
        }
    }
    
    @IBAction func reviewButtonPressed(_ sender: UIButton) {

        guard let url = URL(string : "itms-apps://itunes.apple.com/app/id1380191234?mt=8&action=write-review") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    //MARK: - Private methods
    
    
    
    private func generateStars() {
        let count = Int(drand48() * 6) + 1 //  1 - 6
        (0...count).forEach { (_) in
            let imageView = UIImageView(image: #imageLiteral(resourceName: "star"))
            
            let size = Int(drand48() * 10 + 26) // 26 - 35
            
            imageView.frame = CGRect(x: 0, y: 0, width: size, height: size)
            
            let animation = CAKeyframeAnimation(keyPath: "position")
            animation.duration = 1.5 + drand48()*2
            animation.path = customPath().cgPath
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            animation.delegate = self
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            animation.setValue(imageView, forKey: "viewToBeRemoved")
            imageView.layer.add(animation, forKey: nil)
            view.addSubview(imageView)
        }
        
        func customPath() -> UIBezierPath {
            let path = UIBezierPath()
            
            
            let yPosition = CGFloat(continueButton.frame.minY)
            //        print(yPosition)
            let xEndPosition = view.bounds.maxX + 20
            
            path.move(to: CGPoint(x: -20, y: yPosition))
            let endPoint = CGPoint(x: xEndPosition, y: yPosition)
            let cp1 = CGPoint(x: xEndPosition * 0.33, y: yPosition - CGFloat(drand48()*100 + 100) )
            let cp2 = CGPoint(x: xEndPosition * 0.66, y: yPosition + CGFloat(drand48()*100 + 100) )
            
            path.addCurve(to: endPoint, controlPoint1: cp1, controlPoint2: cp2)
            
            return path
        }
    }
    
    private func showModal() {
        
        modalContainer.isHidden = false
        modalContainer.alpha = 0
        modalView.transform = modalView.transform.translatedBy(x: 0, y: -self.view.bounds.size.height)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
                self.modalContainer.alpha = 0.5
            }) { (sucsess) in
                showModalView()
        }
        
        func showModalView() {
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.modalContainer.alpha = 1
                self.modalView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 70).scaledBy(x: 0.9, y: 0.9)
            }) { (sucsess) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.modalView.transform = CGAffineTransform.identity
                }) { (sucsess) in
                    print("Done ")
                }    
            }
        }
        
    }
    
    private func hideModal() {

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.modalView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 70).scaledBy(x: 0.9, y: 0.9)
        }) { (sucsess) in
            hideModalView()
        }
        
        func hideModalView() {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.modalView.transform = self.modalView.transform.translatedBy(x: 0, y: -self.view.bounds.size.height)
                self.modalContainer.alpha = 0.5
            }) { (sucsess) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.modalContainer.alpha = 0
                }) { (sucsess) in
                    print("Done ")
                    self.modalContainer.isHidden = true
                }
            }
        }
        
    }

    
    // MARK: - Navigation
    
    @IBAction func unwindFromSetup(_ sender: UIStoryboardSegue) {

        if gameState != nil {
            continueButton.isHidden = false
        } else {
            continueButton.isHidden = true
        }
        
        
        if sender.source is TeamScoreViewController {
//            Globals.gameWins = 20
            if Globals.gameWins >= 1 && (Globals.gameWins % 4 == 0 || Globals.gameWins == 1) &&
                !Globals.ratingRequestRejected {
                // show rating request window
                
                if #available(iOS 10.3, *) {
                    SKStoreReviewController.requestReview()
                    // обновлять число игр
                    DataManager.updateGameWins()
                    Analytics.logEvent("review_request_system", parameters: [:])
                }
            }

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let to = segue.destination as? TeamScoreViewController {
            to.gameState = gameState
            to.gameSettings = DataManager.loadSettings()
        }
    }
    
}

extension UIViewController {
    func startBoounce(for viewsArray: [UIView]){
        for (index, subview) in viewsArray.enumerated() {
            let subDelay = Double(index)*0.05 + 0.3
            DispatchQueue.main.asyncAfter(deadline: .now() + subDelay) {
                self.makeBounce(subview)
            }
        }
    }
    
    private func makeBounce(_ view: UIView) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {
            view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (success) in
            makeAnotherBounce(view)
        }
        
        func makeAnotherBounce(_ view: UIView) {
            UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveLinear], animations: {
                view.transform = CGAffineTransform(scaleX: 0.88, y: 0.88)
            }) { (success) in
                makeAnotherOneBounce(view)
            }
        }
        
        func makeAnotherOneBounce(_ view: UIView) {
            UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveLinear], animations: {
                view.transform = CGAffineTransform(scaleX: 1.06, y: 1.06)
            }) { (success) in
                makeAnotherOneMoreBounce(view)
            }
        }
        
        func makeAnotherOneMoreBounce(_ view: UIView){
            UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveLinear], animations: {
                view.transform = CGAffineTransform(scaleX: 0.97, y: 0.97)
            }) { (success) in
                makeAnotherOneMoreTimeBounce(view)
            }
        }
        
        func makeAnotherOneMoreTimeBounce(_ view: UIView){
            UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction, .curveEaseIn], animations: {
                view.transform = CGAffineTransform.identity
            }) { (success) in
                
            }
        }
    }
}

extension StartScreenViewController:CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if let view = anim.value(forKey: "viewToBeRemoved") as? UIView {
            view.removeFromSuperview()
        }
    }
    
}

extension StartScreenViewController: GADInterstitialDelegate {
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-8881122072219311/6229656220")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        print("ad request")
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
//    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialDidDismissScreen")
//    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
