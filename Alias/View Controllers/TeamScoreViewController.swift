//
//  TeamScoreViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 19.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMobileAds
import FirebaseAnalytics
import FBSDKCoreKit

class TeamScoreViewController: UIViewController {
    
    // MARK: - Types
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Public Properties
    var isGameDone = false
    var isStarsGoing = false
    var isNewRoundLoop = false
    var gameSettings: GameSettings? = nil
    var gameState: GameState? = nil
    
    // MARK: - Private Properties
    var gameOver: AVAudioPlayer? = nil
    private var interstitial:GADInterstitial!
    
    
    // MARK: - Initializers
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        
        if let index = gameState?.currentTeamIndex {
            teamLabel.text = gameState?.teamInGame[index].teamName
        }
        if isGameDone {
            setWinScreen()
            generateStars()
            DataManager.deleteSavedState()
            
            if let gameSettings = gameSettings, let gameState = gameState {
                var params = [String: Any]()
                params["teams_count"] = "\(gameState.teamInGame.count)"
                params["quest_frequency"] = "\(gameSettings.questFrequency.rawValue)"
                params["last_word_for_all"] = "\(gameSettings.isLastWordForAll)"
                params["round_time"] = "\(gameSettings.roundDuration)"
                params["word_penalty"] = "\(gameSettings.isWordPenaltyOn)"
                params["words_for_win"] = "\(gameSettings.wordsForWin)"
                
                Analytics.logEvent("game_finished", parameters: params)
                
                AppEvents.logEvent(.achievedLevel)
            }
            
            
        } else {
            if isNewRoundLoop && Globals.gameRoundLoopCount > 0 && Globals.gameRoundLoopCount % 2 == 0 {
                print("Show ad")
                interstitial = createAndLoadInterstitial()
                startButton.layer.opacity = 0
                
            }
        }
        
        prepareSounds()
        if gameOver != nil && isGameDone == true {
            gameOver!.play()
        }
        //        startButton.imageView?.contentMode = .scaleAspectFit
        
        //        tableView.estimatedRowHeight = 44
        //        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        interstitial = nil
        
    }
    
    // MARK: - Public methods
    
    // MARK: - IBActions
    @IBAction func startButtonPressed(_ sender: UIButton) {
        
        if !isGameDone {
            performSegue(withIdentifier: "goToGameRound", sender: self)
        } else {
            performSegue(withIdentifier: "unwindToMainScreen", sender: self)
        }
        
    }
    
    // MARK: - Private Methods
    
    private func setWinScreen() {
        
        if var teams = gameState?.teamInGame {
            teams = teams.sorted(by: {$0.score! > $1.score!})
            gameState?.teamInGame = teams
            statusLabel.text = "Победила команда"
            teamLabel.text = teams[0].teamName
            startButton.setImage(#imageLiteral(resourceName: "newGameButton"), for: .normal)
            
            
            titleLabel.text = "Ура, победа!"
            
//            if !isStarsGoing {
//                generateStars()
//            }
            
            
        }
        
    }
    
    private func generateStars() {
        
        isStarsGoing = true
        
        let count = Int(drand48() * 6) + 1 //  1 - 6
        (0...count).forEach { (_) in
            let imageView = UIImageView(image: #imageLiteral(resourceName: "star"))
            
            let size = Int(drand48() * 10 + 26) // 26 - 35
            
            imageView.frame = CGRect(x: 0, y: 0, width: size, height: size)
            
            let animation = CAKeyframeAnimation(keyPath: "position")
            animation.duration = 0.8 + drand48()*1.8
            animation.path = customPath().cgPath
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            animation.delegate = self
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            animation.setValue(imageView, forKey: "viewToBeRemoved")
            imageView.layer.add(animation, forKey: nil)
            view.addSubview(imageView)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.4) {
            self.generateStars()
        }
        
        func customPath() -> UIBezierPath {
            let path = UIBezierPath()
            
            
            let yPosition = CGFloat(teamLabel.frame.minY)
            //        print(yPosition)
            let xEndPosition = view.bounds.maxX + 20
            
            path.move(to: CGPoint(x: -20, y: yPosition))
            let endPoint = CGPoint(x: xEndPosition, y: yPosition)
            let cp1 = CGPoint(x: xEndPosition * 0.33, y: yPosition - CGFloat(drand48()*100 + 100) )
            let cp2 = CGPoint(x: xEndPosition * 0.66, y: yPosition + CGFloat(drand48()*100 + 100) )
            
            path.addCurve(to: endPoint, controlPoint1: cp1, controlPoint2: cp2)
            
            return path
        }
    }
    
    private func prepareSounds() {
        if let isMusicOn =  gameSettings?.isMusicOn {
            if isMusicOn == true {
                gameOver = DataManager.loadSound(name: "gameOver", type: "wav")
            }
        }
    }
    
    /// Shows main button.
    /// Used after adView did load or failed to load
    private func showStartButton(_ afterDelay: Float) {
        UIView.animate(withDuration: 0.3, delay: TimeInterval(afterDelay), options: [], animations: {
             self.startButton.layer.opacity = 1
        })
        
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Если загружаем экран раунда - передаем настройки + сохраняем их, и передаем состояние с добавленным набором слов
        if let dest = segue.destination as? GameRoundViewController {
            //            print("Save data")
//            print(gameState?.teamInGame ?? "нет команд для игры ")
            
            dest.gameSettings = self.gameSettings
            //            gameState?.wordsInGame = ArrayDataController(withData: wordsPacks![selectedPackId].words!)
//            print(gameState?.wordsInGame ?? [])
            dest.gameState = gameState
            
        } else if let dest = segue.destination as? StartScreenViewController {
            
            if isGameDone == true {
                gameState = nil // раскоментировать потом
//                DataManager.deleteState()
                print("gamestate == nil")
            }
            
            dest.gameState = gameState
        } 
    }
    
}

extension TeamScoreViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameState?.teamInGame.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamScoreCell") as! TeamScoreTableViewCell
        
        
        let rowIndex = indexPath.row
        cell.teamName.text = gameState?.teamInGame[rowIndex].teamName
        cell.teamScore.text = "\( (gameState?.teamInGame[rowIndex].score)! )"
        
        if isGameDone == true {
            cell.teamStatus.text = (rowIndex == 0 ? "Команда победила" : "\(rowIndex+1)-е место" )
//            print(rowIndex)
        } else {
            if let index = gameState?.currentTeamIndex {
                if rowIndex < index  {
                    cell.teamStatus.text = "Уже играла"
                } else if index == rowIndex {
                    cell.teamStatus.text = "Готовится играть"
                } else {
                    cell.teamStatus.text = "Раунд в запасе"
                }
            }
        }
        
        return cell
    }
    
    
    
}

extension TeamScoreViewController : UITableViewDelegate {
    
}


extension TeamScoreViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if let view = anim.value(forKey: "viewToBeRemoved") as? UIView {
            view.removeFromSuperview()
        }
    }
    
}

extension TeamScoreViewController: GADInterstitialDelegate {
    func createAndLoadInterstitial() -> GADInterstitial {
//        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-8881122072219311/6229656220")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        print("ad request")
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        interstitial = createAndLoadInterstitial()
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if self.interstitial.isReady {
                self.interstitial.present(fromRootViewController: self)
                self.showStartButton(0.5)
            } else {
                self.showStartButton(0)
            }
            
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        showStartButton(0)
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
//        func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//            print("interstitialDidDismissScreen")
//            interstitial.delegate = nil
//        }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}
