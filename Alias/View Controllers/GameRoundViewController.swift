//
//  GameRoundViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 19.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import AVFoundation

class GameRoundViewController: UIViewController {
    
    // MARK: - Types
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backView1: UIView!
    @IBOutlet weak var backView2: UIView!

    @IBOutlet weak var modalCardlView: UIView!
    @IBOutlet weak var modalContainer: UIView!
    @IBOutlet weak var modalView: UIView!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var questCardView: UIView!
    @IBOutlet weak var wordCardView: UIView!
    @IBOutlet weak var wordCardViewForAnimation: UIView!
    
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var wordLabelForAnimation: UILabel!
    @IBOutlet weak var questLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var trueFalseButtonsView: UIStackView!
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    // MARK: - Public Properties
    
    var data: [String] = []
    
    var gameSettings: GameSettings? = nil
    var gameState: GameState? = nil
    var questDataController: ArrayDataController<String>!
    var timer = Timer()
    
    var words: [(String, Bool)] = []
    var quest: String? = nil
    
    var lastWordWinnerTeamIndex: Int?
    // MARK: - Private Properties
    
    var timerTick:AVAudioPlayer? = nil
    var positiveSwipe:AVAudioPlayer? = nil
    var negativeSwipe:AVAudioPlayer? = nil
    var roundStart:AVAudioPlayer? = nil
    var roundEnd:AVAudioPlayer? = nil
    
    // MARK: - Initializers
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
        if let teams = gameState?.teamInGame {
            data = teams.map {$0.teamName}
            tableHeight.constant = CGFloat(teams.count * 50)
        }
        
        // TODO: Пофиксить рамки
        modalCardlView.layer.borderWidth = 6
        modalCardlView.layer.borderColor = Globals.yellowColor
        
        questDataController = ArrayDataController(withData: DataManager.loadQuests())
        
        prepareSounds()
        prepareScreen()
    }
    
    // MARK: - Public methods
    
    // MARK: - IBActions
    @IBAction func noOneButtonTapped(_ sender: UIButton) {
        hideModal()
        words.append( (wordLabel.text!, false) )
        performSegue(withIdentifier: "goToRoundScoreScreen", sender: self)
    }
    
    @IBAction func falseButtonPressed(_ sender: UIButton) {
        flipNewCard(false)
    }
    @IBAction func trueButtonPressed(_ sender: UIButton) {
        flipNewCard(true)
    }
    
    @IBAction func startGameButtonPressed(_ sender: Any) {
        if let sender = sender as? UIButton {
            
            if roundStart != nil {
                roundStart!.play()
            }
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                sender.alpha = 0
                sender.transform = CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
                
                self.questLabel.alpha = 0.3
                self.descriptionLabel.alpha = 0
                
                self.trueFalseButtonsView.alpha = 1
                self.trueFalseButtonsView.transform = CGAffineTransform.identity
                
            }, completion: { (success) in
                sender.isHidden = true
                self.trueFalseButtonsView.isHidden = false
                
                if self.quest != nil {
                    self.descriptionLabel.text? = self.questLabel.text!
                }
                
                
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
                    self.questCardView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.view.bounds.size.height/2)
                    self.questLabel.alpha = 0
                    self.descriptionLabel.alpha = 1
                }, completion: { (success) in
                    self.questCardView.alpha = 0
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
                })
                
            })
            
        }
    }
    
    // MARK: - Private Methods
    
    private func showModal() {
        
        modalContainer.isHidden = false
        modalContainer.alpha = 0
        modalView.transform = modalView.transform.translatedBy(x: 0, y: -self.view.bounds.size.height)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.modalContainer.alpha = 0.5
        }) { (sucsess) in
            showModalView()
        }
        
        func showModalView() {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.modalContainer.alpha = 1
                self.modalView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 70).scaledBy(x: 0.9, y: 0.9)
            }) { (sucsess) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.modalView.transform = CGAffineTransform.identity
                }) { (sucsess) in
                    print("Done ")
                }
            }
        }
        
    }
    
    private func hideModal() {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.modalView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 70).scaledBy(x: 0.9, y: 0.9)
        }) { (sucsess) in
            hideModalView()
        }
        
        func hideModalView() {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.modalView.transform = self.modalView.transform.translatedBy(x: 0, y: -self.view.bounds.size.height)
                self.modalContainer.alpha = 0.5
            }) { (sucsess) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.modalContainer.alpha = 0
                }) { (sucsess) in
                    print("Done ")
                    self.modalContainer.isHidden = true
                }
            }
        }
        
    }
    
    private func flipNewCard(_ isCorrect: Bool) {
        
        view.layer.removeAllAnimations()
        wordCardViewForAnimation.layer.removeAllAnimations()
        
        words.append( (wordLabel.text!, isCorrect) )
        
        let word = gameState?.wordsInGame?.randomItem
        
        wordLabelForAnimation.text? = wordLabel.text!
        wordCardViewForAnimation.transform = CGAffineTransform.identity
        self.wordCardViewForAnimation.backgroundColor = UIColor(red: 0/255, green: 58/255, blue: 95/255, alpha: 1)
//        wordCardViewForAnimation.alpha = 1
        
        wordLabel.text?  = word!
        
        let xOffset = CGFloat(isCorrect ? 350.0 : -350.0)
        let rotateBy = CGFloat(isCorrect ? 1 : -1)
        let color = isCorrect ? UIColor.green : UIColor.red
        if isCorrect {
            if positiveSwipe != nil {
                positiveSwipe!.currentTime = 0
                positiveSwipe!.play()
            }
        } else {
            if negativeSwipe != nil {
                negativeSwipe!.currentTime = 0
                negativeSwipe!.play()
            }
        }
        
        if  Int(timerLabel.text!)! > 0 {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.wordCardViewForAnimation.transform = CGAffineTransform.identity.translatedBy(x: xOffset, y: 50)
                    .scaledBy(x: 0.5, y: 0.5).rotated(by: rotateBy)
                self.wordCardViewForAnimation.backgroundColor = color
            })
        } else {
            performSegue(withIdentifier: "goToRoundScoreScreen", sender: self)
        }
        
    }
    
    private func getQuest() -> String? {
        
        let random = arc4random_uniform(10)
        let frequency = gameSettings?.questFrequency
        
        switch frequency! {
        case .never: break
        case .rarely:
            if random < 3 {
                quest = questDataController.randomItem!
            }
        case .often:
            if random < 7 {
                quest = questDataController.randomItem!
            }
        case .always:
            quest = questDataController.randomItem!
        }
        
        print("Получен новый квест: \(quest ?? "nill")")
        
        return quest
    }
    
    private func prepareScreen() {
        
        if let quest = getQuest() {
            questLabel.text?  = quest
            descriptionLabel.text? = "Раунд с заданием!"
        }
        
        timerLabel.text? = "\(gameSettings?.roundDuration ?? 60)"
        
        //back cards rotation
        backView1.transform = CGAffineTransform.identity.rotated(by: 0.05).translatedBy(x: 3, y: 4)
        backView2.transform = CGAffineTransform.identity.rotated(by: -0.08).translatedBy(x: -3, y: 0)
        
        backView1.layer.borderWidth = 6
        backView1.layer.borderColor = Globals.darkBlueColor
        
        backView2.layer.borderWidth = 6
        backView2.layer.borderColor = Globals.darkBlueColor
        
        questCardView.backgroundColor = UIColor(cgColor: Globals.darkBlueColor)
        wordCardView.backgroundColor = UIColor(cgColor: Globals.darkBlueColor)
        wordCardViewForAnimation.backgroundColor = UIColor(cgColor: Globals.darkBlueColor)
        

        wordLabel.superview?.backgroundColor = UIColor.init(patternImage: UIImage(named: "patternWhite")!)
        wordLabelForAnimation.superview?.backgroundColor = UIColor.init(patternImage: UIImage(named: "patternWhite")!)
        
        questLabel.superview?.backgroundColor = UIColor.init(patternImage: UIImage(named: "patternBlue")!)
        
        
        
        
        trueFalseButtonsView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 200)
        trueFalseButtonsView.alpha = 0
        trueFalseButtonsView.isHidden = true
        startGameButton.transform = CGAffineTransform.identity
        startGameButton.alpha = 1
        startGameButton.isHidden = false
        
        questCardView.transform = CGAffineTransform.identity
        questCardView.alpha = 1
        
        questLabel.alpha = 1
        
        wordCardViewForAnimation.transform = CGAffineTransform.identity.translatedBy(x: 700, y: 50)
        
        let word = gameState?.wordsInGame?.randomItem
        wordLabel.text?  = word!
        
    }
    
    private func prepareSounds() {
        if let isMusicOn =  gameSettings?.isMusicOn {
            if isMusicOn == true {
                timerTick = DataManager.loadSound(name: "timerTick", type: "wav")
                positiveSwipe = DataManager.loadSound(name: "positiveSwipe", type: "wav")
                negativeSwipe = DataManager.loadSound(name: "negativeSwipe", type: "wav")
                roundStart = DataManager.loadSound(name: "roundStart", type: "wav")
                roundEnd = DataManager.loadSound(name: "roundEnd", type: "wav")
            }
        }
    }
    
    @objc private func timerAction() {
        var roundTime = Int(timerLabel.text!)!
        
        roundTime -= 1
        if roundTime < 0 {
            timer.invalidate()
            print("End round")
            
            // sound effect
            if roundEnd != nil {
                roundEnd!.currentTime = 0
                roundEnd!.play()
            }
            if (gameSettings?.isLastWordForAll)! {
                showModal()
                let generator = UINotificationFeedbackGenerator()
                generator.prepare()
                generator.notificationOccurred(.success)
            } else {
                //performSegue(withIdentifier: "goToRoundScoreScreen", sender: self)
            }
            
        } else {
            if (1...5).contains(roundTime) {
                if timerTick != nil {
                    timerTick!.currentTime = 0
                    timerTick!.play()
                }
            }
            timerLabel.text? = "\(roundTime)"
        }

    }
    
    // MARK: - Navigation
    
    @IBAction func unwindToGameRound(_ sender: UIStoryboardSegue) {
        print("unwind to game")
        prepareScreen()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? RoundScoreViewController {
            
//            print(gameState?.teamInGame ?? [])
            
            
            
            dest.gameSettings = gameSettings
            dest.gameState = gameState
            dest.words = words
            dest.quest = quest
            dest.lastWordWinnerTeamIndex = lastWordWinnerTeamIndex
        }
    }
    
}

extension GameRoundViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "lastWordTeamCell") as! LastWordTeamTableViewCell
        cell.teamName.text = data[indexPath.row]
        
        return cell
    }
    
    
}

extension GameRoundViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let index = indexPath.row
        print(index)
        lastWordWinnerTeamIndex = index
        let correct = (index == gameState?.currentTeamIndex)
        words.append( (wordLabel.text!, correct) )
        
//        if  gameState?.teamInGame[index].score != nil {
//            gameState?.teamInGame[index].score! += 1
//        }
        hideModal()
        
        performSegue(withIdentifier: "goToRoundScoreScreen", sender: self)
        
    }
    
    
}
