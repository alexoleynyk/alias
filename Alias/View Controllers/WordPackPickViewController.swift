//
//  WordPackPickViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 20.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class WordPackPickViewController: UIViewController {
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Public Properties
    var wordsPacks: [WordsPack] = []
    var selectedPackId = -1
    var gameState: GameState? = nil

    // MARK: - Private Properties
    
    // MARK: - Initializers
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
      
        DispatchQueue.global(qos: .userInitiated).async {
            self.wordsPacks = DataManager.loadWordPacks()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        
    }
    
    // MARK: - Public methods
    
    // MARK: - IBActions
    
    // MARK: - Private Methods
   
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? SettingsViewController {
//            dest.selectedPackId =  self.selectedPackId
            dest.wordsPacks = self.wordsPacks
            dest.gameState = self.gameState
            dest.selectedPackId = self.selectedPackId
        }
    }
    
    @IBAction func unwindFromSettingsToWordPacks(_ sender: UIStoryboardSegue) {
        if sender.source is SettingsViewController {
            print("Hello from SettingView!")
        }
    }
    
}

extension WordPackPickViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wordsPacks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wordPackCell") as! WordPackTableViewCell
        
        let dataElement = wordsPacks[indexPath.row]
        
        
        cell.packName!.text = dataElement.packName
        cell.packDescription!.text = dataElement.packDescription
        cell.row = indexPath.row
        cell.packWordsCount!.text = "\(dataElement.words?.count ?? 0)"
        cell.delegate = self
        cell.words = dataElement.words!
//        print(dataElement.words!)
        return cell
    }
    
}

extension WordPackPickViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // настройка тени под каждой карточкой
//        cell.layer.masksToBounds = false
//        let size = cell.bounds.size
        
        //Создание пути для тени
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: 10.0,y: size.height - 15.0))
//        path.addLine(to: CGPoint(x: size.width - 10, y: size.height - 15.0))
//        path.addLine(to: CGPoint(x: size.width - 10, y: size.height + 4))
//        path.addLine(to: CGPoint(x: 10.0, y: size.height + 4))
//        path.close()
//        
//        let view = cell.subviews[0]
//        view.layer.shadowOpacity = 0.6
//        view.layer.shadowRadius = 5.0
//        view.layer.shadowPath = path.cgPath
    }
}

extension WordPackPickViewController: WordPackTableViewCellDelegate {
    func chooseWordPack(row: Int) {
        // Выбор нужного набора слов и сохранение в настройках
        selectedPackId = row
        gameState?.wordsInGame = ArrayDataController(withData: wordsPacks[selectedPackId].words!)
        performSegue(withIdentifier: "showSettingViewController", sender: Any?.self)
        print(row)
        
    }
    
    
    
    
}
