//
//  RoundScoreViewController.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 19.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class RoundScoreViewController: UIViewController {
    
    // MARK: - Constants
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var teamScoreLabel: UILabel!
    
    @IBOutlet weak var modalContainer: UIView!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var modalCardlView: UIView!
    @IBOutlet weak var teamsTableView: UITableView!
    @IBOutlet weak var teamsTableHeight: NSLayoutConstraint!
    
    // MARK: - Public Properties
    
    var gameSettings: GameSettings? = nil
    var gameState: GameState? = nil
    var words: [(String, Bool)] = []
    var quest: String? = nil
    var questBonus: Int = 0
    var scoreStep = 1
    var score = 0 {
        didSet {
            teamScoreLabel.text =  "Набрано очков: \(score)"
        }
    }
    var lastWordWinnerTeamIndex: Int?
    var isLastWordForAll = false
    var teams: [String] = []
    
    // MARK: - Private Properties
    
    // MARK: - Initializers
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        
        
        teamsTableView.dataSource = self
        teamsTableView.delegate = self
        teamsTableView.isScrollEnabled = false
        if let teamsData = gameState?.teamInGame {
            teams = teamsData.map {$0.teamName}
            teamsTableHeight.constant = CGFloat(teamsData.count * 50)
        }
        modalCardlView.layer.borderWidth = 6
        modalCardlView.layer.borderColor = Globals.yellowColor
        

        var penalty = 0
        if gameSettings?.isWordPenaltyOn == true {
            penalty = 1
            scoreStep = 2
        }
        
        
        isLastWordForAll = gameSettings?.isLastWordForAll ?? false

        for (index, word) in words.enumerated() {
            if (index == words.count - 1)  && isLastWordForAll { continue }
            if word.1 {
                score += 1
            } else {
                score -= penalty
            }
        }
        
        if let currentIndex = gameState?.currentTeamIndex, let winnerTeam = lastWordWinnerTeamIndex {
            if currentIndex == winnerTeam {
                score += 1
            }
        }
        
//        if isLastWordForAll
        
//        if let index = gameState?.currentTeamIndex {
//            gameState?.teamInGame[index].score = score + (gameState?.teamInGame[index].score)!
//            DataManager.saveState(gameState!)
//        }
        
        if let quest = quest {
            
            questBonus = Int(arc4random_uniform(UInt32(3))) + 7
            words.insert(("Задание (+\(questBonus)): \(quest)", false), at: 0)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = gameState?.currentTeamIndex {
            teamNameLabel.text = gameState?.teamInGame[index].teamName
            teamScoreLabel.text =  "Набрано очков: \(score)"
        }
        
    }
    
    // MARK: - Public methods
    
    // MARK: - IBActions
    
    @IBAction func noOneButtonTapped(_ sender: UIButton) {
        if let currentTeamIndex = gameState?.currentTeamIndex, let winnerTeamIndex = lastWordWinnerTeamIndex {
            if currentTeamIndex == winnerTeamIndex {
                score -= 1
            }
        }
        lastWordWinnerTeamIndex = nil
        
        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? WordScoreTableViewCell {
            if let button = cell.statusButton as? BounceButton {
                button.makeBounceCall(isSelected: false)
            }
        }
        hideModal()
    }
    
    // MARK: - Private Methods
    
    private func showModal() {
        
        modalContainer.isHidden = false
        modalContainer.alpha = 0
        modalView.transform = modalView.transform.translatedBy(x: 0, y: -self.view.bounds.size.height)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.modalContainer.alpha = 0.5
        }) { (sucsess) in
            showModalView()
        }
        
        func showModalView() {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.modalContainer.alpha = 1
                self.modalView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 70).scaledBy(x: 0.9, y: 0.9)
            }) { (sucsess) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.modalView.transform = CGAffineTransform.identity
                }) { (sucsess) in
                    print("Done ")
                }
            }
        }
        
    }
    
    private func hideModal() {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.modalView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: 70).scaledBy(x: 0.9, y: 0.9)
        }) { (sucsess) in
            hideModalView()
        }
        
        func hideModalView() {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                self.modalView.transform = self.modalView.transform.translatedBy(x: 0, y: -self.view.bounds.size.height)
                self.modalContainer.alpha = 0.5
            }) { (sucsess) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.modalContainer.alpha = 0
                }) { (sucsess) in
                    print("Done ")
                    self.modalContainer.isHidden = true
                }
            }
        }
        
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextVC = segue.destination as? TeamScoreViewController {
            
            if let index = gameState?.currentTeamIndex {
                gameState?.teamInGame[index].score = score + (gameState?.teamInGame[index].score)!
                
                if let winnerTeamIndex = lastWordWinnerTeamIndex {
                    if index != winnerTeamIndex {
                        gameState?.teamInGame[winnerTeamIndex].score = 1 + (gameState?.teamInGame[winnerTeamIndex].score)!
                    }
                }
                
                if index < (gameState?.teamInGame.count)! - 1 {
                    gameState?.currentTeamIndex += 1
                } else {
    
                    if let teams = gameState?.teamInGame, let wordsForWin = gameSettings?.wordsForWin {
                        
                        // проверка на победу
                        let scores = teams.map({$0.score!})
                            .filter({$0 >= wordsForWin})
                            .sorted(by: {$0 > $1})
                        
                        if scores.count == 1 || (scores.count > 1 && scores[0] != scores[1])  {
                            nextVC.isGameDone = true
                            DataManager.updateGameWins()
                        } else {
                            gameState?.currentTeamIndex = 0
                            
                            // прошел 1 игрвой круг
                            Globals.gameRoundLoopCount += 1
                            nextVC.isNewRoundLoop = true
                        }
                        
                    }
                }
            }

            DataManager.saveState(gameState!)
            nextVC.gameState = gameState
            nextVC.gameSettings = gameSettings
        }
    }
 

}

extension RoundScoreViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case self.tableView:
            if gameSettings?.isLastWordForAll == true {
                return 2
            } else {
                return 1
            }
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case self.tableView:
            if section == 0 {
                if gameSettings?.isLastWordForAll == true {
                    return words.count - 1
                } else {
                    return words.count
                }
                
            } else {
                return 1
            }
        default:
           return teams.count
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch tableView {
        case self.tableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "wordScroreTableCell") as! WordScoreTableViewCell
            var (word, status): (String, Bool) = ("", false)
            if gameSettings?.isLastWordForAll == false {
                (word, status) = words[indexPath.row]
            } else {
                if indexPath.section == 0 {
                    (word, status) = words[indexPath.row]
                } else {
                    (word, status) = words.last!
                }
            }
            
            cell.wordTitle.text = word
            cell.statusButton.isSelected = status
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "lastWordTeamCell") as! LastWordTeamTableViewCell
            cell.teamName.text = teams[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch tableView {
        case self.tableView:
            if section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "lastWordTableCell") as! LastWordTableViewCell
                return cell
            }
            return nil
        default:
            return nil
        }
    }
}

extension RoundScoreViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row
        let section = indexPath.section
        
        // Таблица с результатами раунда
        switch tableView {
        case self.tableView:
            
            if let cell = tableView.cellForRow(at: indexPath) as? WordScoreTableViewCell {
                let isStatusPositive = cell.statusButton.isSelected
                
                if section == 0 {
                    
                    if quest != nil && index == 0 {
                        score += (isStatusPositive ? -questBonus : questBonus)
                    } else {
                        score += (isStatusPositive ? -scoreStep : scoreStep)
                    }
                    //            cell.statusButton.isSelected = !isStatusPositive
                    if let button = cell.statusButton as? BounceButton {
                        button.makeBounceCall(isSelected: !isStatusPositive)
                    }
                } else {
                    showModal()
                }
                
            }
            
        // Таблица выбора комманды отгадавшей последнее слово
        default:
            var correct = true
            if let currentTeamIndex = gameState?.currentTeamIndex {
                
                if let winnerTeamIndex = lastWordWinnerTeamIndex {
                    if currentTeamIndex == index && currentTeamIndex != winnerTeamIndex  {
                        score += 1
                    } else if index != winnerTeamIndex  {
                        score += -1
                        correct = false
                    }
                    
                } else {
                    if currentTeamIndex == index {
                        score += 1
//                        correct = true
                    }
                }
            }
            lastWordWinnerTeamIndex = index
            
            if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? WordScoreTableViewCell {
                if let button = cell.statusButton as? BounceButton {
                    button.makeBounceCall(isSelected: correct)
                }
            }
            
            hideModal()
        }
        
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch tableView {
        case self.tableView:
            if section == 0 {
                return 0.0
            } else {
                return 40.0
            }
        default:
            return 0.0
        }
    }
    
    

    
}
