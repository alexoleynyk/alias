//
//  AppDelegate.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 11.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Firebase
import Crashlytics
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // test id
//        GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544~1458002511")
        
        // my id
        GADMobileAds.configure(withApplicationID: "ca-app-pub-8881122072219311~3221970073")
        FirebaseApp.configure()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Globals.gameWins = DataManager.loadGameWins()
        Globals.ratingRequestRejected = DataManager.loadRatingRequest()
        
        if let startScreenController = window?.rootViewController as? StartScreenViewController {
            startScreenController.gameState =  DataManager.loadState()
        }
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
    }
}

