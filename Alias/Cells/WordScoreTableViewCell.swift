//
//  WordScoreTableViewCell.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 19.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class WordScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var wordTitle: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
