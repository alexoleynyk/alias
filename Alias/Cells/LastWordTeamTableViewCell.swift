//
//  LastWordTeamTableViewCell.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 19.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

class LastWordTeamTableViewCell: UITableViewCell {

    @IBOutlet weak var teamName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let view = self.contentView.subviews.first {
            view.layer.cornerRadius = 10
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
