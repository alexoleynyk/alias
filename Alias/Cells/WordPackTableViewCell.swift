//
//  WordPackTableViewCell.swift
//  Alias
//
//  Created by Oleksii Oliinyk on 20.03.2018.
//  Copyright © 2018 Oleksii Oliinyk. All rights reserved.
//

import UIKit

protocol WordPackTableViewCellDelegate {
    func chooseWordPack(row: Int)
}

class WordPackTableViewCell: UITableViewCell {

    @IBOutlet weak var packName: UILabel!
    @IBOutlet weak var packDescription: UILabel!
    @IBOutlet weak var packExamples: UILabel!
    @IBOutlet weak var packWordsCount: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    var delegate: WordPackTableViewCellDelegate?
    var row: Int!
    var words: [String]! {
        didSet {
            let wordController = ArrayDataController(withData: words)
            let examples = "\(wordController.randomItem!), \(wordController.randomItem!), \(wordController.randomItem!)"
            packExamples.text = examples
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    
    
    @IBAction func chooseButtonClicked(_ sender: UIButton) {
        if delegate != nil {
            // unwindforSegue and set WordPack
            delegate?.chooseWordPack(row: self.row)
        }
    }

}
