//
//  LastWordTableViewCell.swift
//  Alias
//
//  Created by Alex Oleynyk on 06.10.2018.
//  Copyright © 2018 Alex Oleynyk. All rights reserved.
//

import UIKit

class LastWordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
